﻿#Requires AutoHotkey v2.0



; if(A_SendMode = "Input")
; 	TrayTip "SetMouseDelay does not work if SendMode is Input"
; SetMouseDelay 1000

delay:=300

#hotif (WinActive("ahk_exe Potion Craft.exe"))
v::
{
distance:=300
Click , , "Left", 1,  "Down"
sleep delay
c:=0
while c < 10
{
	MouseMove 0, -(distance+c*20), 50, "R"
	sleep delay
	c++
	MouseMove 0, distance+c*20, 50, "R"
	sleep delay
}
Click , , "Left", 1,  "Up"
}

b::
{
distance:=300
Click , , "Left", 1,  "Down"
sleep delay
c:=0
while c < 10
{
	MouseMove -(distance+c*20), 0 , 50, "R"
	sleep delay
	MouseMove distance+c*20, 0, 50, "R"
	sleep delay
	c++
}
Click , , "Left", 1,  "Up"
}
#hotif
